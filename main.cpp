#include <iostream>

int main(int argc, char* argv[])
{
	double array[] = {1.0, 2.0, 3.3, 4.5, 5.7, 6.7, 1.1};
	int bytes = sizeof(array);
	int dsize = sizeof(double);
	int length = bytes / dsize;
	std::cout<<"Array has "<<bytes<<" bytes."<<std::endl
	<<"Double has "<<dsize<<" bytes"<<std::endl<<
	"Length has "<<length<<" bytes."<<std::endl;

	std::cout << "there are " << argc << " arguments given to this program" << std::endl;

	std::cout << "The arguments to this program are:\n";
	for (int i = 0; i < argc; i++) {
		std::cout << "arg" << i << " => '" << argv[i] << "'"
			<< std::endl;
	}

	std::cout << "What happens when I access the element"
		"indicated by the variable 'length'?" << std::endl;

	std::cout << array[length] << std::endl;
}
